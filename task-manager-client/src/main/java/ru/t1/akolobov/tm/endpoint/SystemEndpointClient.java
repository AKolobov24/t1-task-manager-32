package ru.t1.akolobov.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.akolobov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.akolobov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.akolobov.tm.dto.response.ApplicationVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

}
