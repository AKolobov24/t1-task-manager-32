package ru.t1.akolobov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.request.TaskGetByIndexRequest;
import ru.t1.akolobov.tm.dto.response.TaskGetByIndexResponse;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class TaskDisplayByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-display-by-index";

    @NotNull
    public static final String DESCRIPTION = "Find task by Index and display.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        TaskGetByIndexRequest request = new TaskGetByIndexRequest();
        request.setIndex(index);
        TaskGetByIndexResponse response = getTaskEndpoint().getByIndex(request);
        @Nullable final Task task = response.getTask();
        if (task == null) throw new TaskNotFoundException();
        displayTask(task);
    }

}
