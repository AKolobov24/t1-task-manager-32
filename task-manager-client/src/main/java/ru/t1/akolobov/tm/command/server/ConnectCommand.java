package ru.t1.akolobov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.command.AbstractCommand;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        try {
            @NotNull IServiceLocator serviceLocator = getServiceLocator();
            @Nullable Socket socket = serviceLocator.getConnectionEndpointClient().connect();
            serviceLocator.getTaskEndpoint().setSocket(socket);
            serviceLocator.getProjectEndpoint().setSocket(socket);
            serviceLocator.getUserEndpoint().setSocket(socket);
            serviceLocator.getDomainEndpoint().setSocket(socket);
            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

}
