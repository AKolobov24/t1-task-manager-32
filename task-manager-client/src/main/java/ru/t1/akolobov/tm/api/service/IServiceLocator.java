package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectEndpointClient getProjectEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskEndPointClient getTaskEndpoint();

    @NotNull
    IUserEndpointClient getUserEndpoint();

    @NotNull
    IDomainEndpointClient getDomainEndpoint();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

}
