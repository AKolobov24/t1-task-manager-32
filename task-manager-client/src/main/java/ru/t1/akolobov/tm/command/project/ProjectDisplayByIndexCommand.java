package ru.t1.akolobov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.request.ProjectGetByIndexRequest;
import ru.t1.akolobov.tm.dto.response.ProjectGetByIndexResponse;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-display-by-index";

    @NotNull
    public static final String DESCRIPTION = "Find project by Index and display.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        ProjectGetByIndexRequest request = new ProjectGetByIndexRequest();
        @NotNull final ProjectGetByIndexResponse response = getProjectEndpoint().getByIndex(request);
        @Nullable final Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
        displayProject(project);
    }

}
