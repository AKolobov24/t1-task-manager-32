package ru.t1.akolobov.tm.endpoint;

import lombok.NoArgsConstructor;
import ru.t1.akolobov.tm.api.endpoint.IEndpointClient;

@NoArgsConstructor
public final class ConnectionEndpointClient extends AbstractEndpointClient implements IEndpointClient {

}
