package ru.t1.akolobov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    @Nullable
    Socket getSocket();

    void setSocket(Socket socket);

    @Nullable
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

}
