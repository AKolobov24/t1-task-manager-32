package ru.t1.akolobov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.DataBackupSaveRequest;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-backup";

    @NotNull
    public static final String DESCRIPTION = "Save backup data to base64 file.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getDomainEndpoint().saveBackup(new DataBackupSaveRequest());
    }

}
