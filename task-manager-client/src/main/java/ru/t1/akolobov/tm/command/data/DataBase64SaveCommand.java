package ru.t1.akolobov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.DataBase64SaveRequest;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-base64";

    @NotNull
    public static final String DESCRIPTION = "Save data to base64 file.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BASE64]");
        getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest());
    }

}
