package ru.t1.akolobov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.DataXmlLoadFasterXmlRequest;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlLoadFasterXmlRequest());
    }

}
