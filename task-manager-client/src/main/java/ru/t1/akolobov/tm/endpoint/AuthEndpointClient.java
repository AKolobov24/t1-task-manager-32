package ru.t1.akolobov.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.akolobov.tm.dto.request.UserLoginRequest;
import ru.t1.akolobov.tm.dto.request.UserLogoutRequest;
import ru.t1.akolobov.tm.dto.request.UserProfileRequest;
import ru.t1.akolobov.tm.dto.response.UserLoginResponse;
import ru.t1.akolobov.tm.dto.response.UserLogoutResponse;
import ru.t1.akolobov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    @Override
    @NotNull
    @SneakyThrows
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
