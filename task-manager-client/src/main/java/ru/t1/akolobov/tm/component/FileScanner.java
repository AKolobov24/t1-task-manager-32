package ru.t1.akolobov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        folder = new File(bootstrap.getPropertyService().getCommandFolder());
    }

    private void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getArgumentCommands();
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 5, TimeUnit.SECONDS);
    }

    private void process() {
        File[] files = folder.listFiles();
        if (files == null || files.length == 0) return;
        for (File file : files) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            if (commands.contains(fileName)) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        init();
    }

}
