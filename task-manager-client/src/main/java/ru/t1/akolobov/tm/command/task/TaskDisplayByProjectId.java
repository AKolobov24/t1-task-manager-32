package ru.t1.akolobov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.request.TaskGetByProjectIdRequest;
import ru.t1.akolobov.tm.dto.response.TaskGetByProjectIdResponse;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskDisplayByProjectId extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-display-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Find tasks by project Id and display task list.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest();
        request.setProjectId(id);
        TaskGetByProjectIdResponse response = getTaskEndpoint().getByProjectId(request);
        @NotNull final List<Task> taskList = response.getTaskList();
        renderTaskList(taskList);
    }

}
