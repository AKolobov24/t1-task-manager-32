package ru.t1.akolobov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.endpoint.IEndpointClient;
import ru.t1.akolobov.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    private Socket socket;

    protected <T> T call(@NotNull final Object data, @NotNull Class<T> clazz) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    @Override
    @Nullable
    public Socket connect() throws IOException {
        socket = new Socket(host, port);
        return socket;
    }

    @Override
    @Nullable
    public Socket disconnect() throws IOException {
        socket.close();
        return socket;
    }

}
