package ru.t1.akolobov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.endpoint.IProjectEndpointClient;
import ru.t1.akolobov.tm.api.endpoint.ITaskEndPointClient;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectEndpointClient getProjectEndpoint() {
        return serviceLocator.getProjectEndpoint();
    }

    @NotNull
    protected ITaskEndPointClient getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void displayProject(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
