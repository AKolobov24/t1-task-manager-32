package ru.t1.akolobov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.akolobov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.akolobov.tm.dto.response.ApplicationVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request);

    @NotNull
    ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request);

}
