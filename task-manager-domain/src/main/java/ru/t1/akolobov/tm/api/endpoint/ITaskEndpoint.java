package ru.t1.akolobov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskGetByProjectIdResponse getByProjectId(@NotNull TaskGetByProjectIdRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clear(@NotNull TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse completeById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse create(@NotNull TaskCreateRequest request);

    @NotNull
    TaskGetByIdResponse getById(@NotNull TaskGetByIdRequest request);

    @NotNull
    TaskGetByIndexResponse getByIndex(@NotNull TaskGetByIndexRequest request);

    @NotNull
    TaskListResponse list(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskStartByIdResponse startById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse updateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateByIndex(@NotNull TaskUpdateByIndexRequest request);

}
