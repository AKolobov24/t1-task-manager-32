package ru.t1.akolobov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse loadBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse loadDataJsonJaxb(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveJaxBResponse saveDataJsonJaxb(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse loadDataXmlJaxb(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveJaxBResponse saveDataXmlJaxb(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse loadDataYaml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse saveDataYaml(@NotNull DataYamlSaveFasterXmlRequest request);

}
