package ru.t1.akolobov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clear(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCompleteByIdResponse completeById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse completeByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    ProjectCreateResponse create(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectGetByIdResponse getById(@NotNull ProjectGetByIdRequest request);

    @NotNull
    ProjectGetByIndexResponse getByIndex(@NotNull ProjectGetByIndexRequest request);

    @NotNull
    ProjectListResponse list(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse startById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse startByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
