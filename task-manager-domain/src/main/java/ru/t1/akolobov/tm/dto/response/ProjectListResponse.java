package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @NotNull
    private List<Project> projectList;

    public ProjectListResponse(@NotNull List<Project> projectList) {
        this.projectList = projectList;
    }

}
