package ru.t1.akolobov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public ProjectListRequest(@Nullable Sort sortType) {
        this.sortType = sortType;
    }

}
