package ru.t1.akolobov.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error! Status is empty or value does not match any picklist value...");
    }

}
