package ru.t1.akolobov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request);

}
