package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<Task> taskList;

    public TaskGetByProjectIdResponse(@NotNull List<Task> taskList) {
        this.taskList = taskList;
    }

}
