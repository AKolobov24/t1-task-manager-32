package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.IUserEndpoint;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.model.User;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    private final IUserService userService;

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request) {
        check(request);
        userService.setPassword(request.getUserId(), request.getPassword());
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        check(request, Role.ADMIN);
        userService.lockUserByLogin(request.getLogin());
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) {
        @NotNull User user = userService.create(
                request.getLogin(),
                request.getPassword(),
                request.getEmail()
        );
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        check(request, Role.ADMIN);
        userService.removeByLogin(request.getLogin());
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) {
        check(request, Role.ADMIN);
        userService.unlockUserByLogin(request.getLogin());
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request) {
        check(request);
        userService.updateUser(
                request.getUserId(),
                request.getFirstName(),
                request.getLastName(),
                request.getMiddleName());
        return new UserUpdateProfileResponse();
    }

}
