package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.model.Project;

public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = getServiceLocator().getProjectService();
        this.projectTaskService = getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeStatusById(@NotNull ProjectChangeStatusByIdRequest request) {
        check(request);
        projectService.changeStatusById(
                request.getUserId(),
                request.getId(),
                request.getStatus()
        );
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) {
        check(request);
        projectService.changeStatusByIndex(
                request.getUserId(),
                request.getIndex(),
                request.getStatus()
        );
        return new ProjectChangeStatusByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectClearResponse clear(@NotNull ProjectClearRequest request) {
        check(request);
        projectService.clear(request.getUserId());
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeById(@NotNull ProjectCompleteByIdRequest request) {
        check(request);
        projectService.changeStatusById(
                request.getUserId(),
                request.getId(),
                Status.COMPLETED
        );
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        check(request);
        projectService.changeStatusByIndex(
                request.getUserId(),
                request.getIndex(),
                Status.COMPLETED
        );
        return new ProjectCompleteByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectCreateResponse create(@NotNull ProjectCreateRequest request) {
        check(request);
        return new ProjectCreateResponse(
                projectService.create(
                        request.getUserId(),
                        request.getName(),
                        request.getDescription()
                )
        );
    }

    @NotNull
    @Override
    public ProjectGetByIdResponse getById(@NotNull ProjectGetByIdRequest request) {
        check(request);
        return new ProjectGetByIdResponse(
                projectService.findOneById(
                        request.getUserId(), request.getId()
                )
        );
    }

    @NotNull
    @Override
    public ProjectGetByIndexResponse getByIndex(@NotNull ProjectGetByIndexRequest request) {
        check(request);
        return new ProjectGetByIndexResponse(
                projectService.findOneByIndex(
                        request.getUserId(),
                        request.getIndex()
                )
        );
    }

    @NotNull
    @Override
    public ProjectListResponse list(@NotNull ProjectListRequest request) {
        check(request);
        return new ProjectListResponse(
                projectService.findAll(
                        request.getUserId(),
                        request.getSortType()
                )
        );
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeById(@NotNull ProjectRemoveByIdRequest request) {
        check(request);
        projectTaskService.removeProjectById(request.getUserId(), request.getId());
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        check(request);
        @Nullable final Project project = projectService.findOneByIndex(request.getUserId(), request.getIndex());
        if (project == null) throw new ProjectNotFoundException();
        projectTaskService.removeProjectById(request.getUserId(), project.getId());
        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startById(@NotNull ProjectStartByIdRequest request) {
        check(request);
        projectService.changeStatusById(
                request.getUserId(),
                request.getId(),
                Status.IN_PROGRESS
        );
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startByIndex(@NotNull ProjectStartByIndexRequest request) {
        check(request);
        projectService.changeStatusByIndex(
                request.getUserId(),
                request.getIndex(),
                Status.IN_PROGRESS
        );
        return new ProjectStartByIndexResponse();
    }

    @Override
    public @NotNull ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request) {
        check(request);
        projectService.updateById(
                request.getUserId(),
                request.getId(),
                request.getName(),
                request.getDescription()
        );
        return new ProjectUpdateByIdResponse();
    }

    @Override
    public @NotNull ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        check(request);
        projectService.updateByIndex(
                request.getUserId(),
                request.getIndex(),
                request.getName(),
                request.getDescription()
        );
        return new ProjectUpdateByIndexResponse();
    }

}
