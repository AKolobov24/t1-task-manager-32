package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.api.service.ITaskService;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Status;

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = getServiceLocator().getTaskService();
        this.projectTaskService = getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindToProject(@NotNull TaskBindToProjectRequest request) {
        check(request);
        projectTaskService.bindTaskToProject(
                request.getUserId(),
                request.getProjectId(),
                request.getTaskId()
        );
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        check(request);
        projectTaskService.unbindTaskFromProject(
                request.getUserId(),
                request.getId()
        );
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    public TaskGetByProjectIdResponse getByProjectId(@NotNull TaskGetByProjectIdRequest request) {
        check(request);
        return new TaskGetByProjectIdResponse(
                taskService.findAllByProjectId(
                        request.getUserId(),
                        request.getProjectId()
                )
        );
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        check(request);
        taskService.changeStatusById(
                request.getUserId(),
                request.getId(),
                request.getStatus()
        );
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        check(request);
        taskService.changeStatusByIndex(
                request.getUserId(),
                request.getIndex(),
                request.getStatus()
        );
        return new TaskChangeStatusByIndexResponse();
    }

    @NotNull
    @Override
    public TaskClearResponse clear(@NotNull TaskClearRequest request) {
        check(request);
        taskService.clear(request.getUserId());
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeById(@NotNull TaskCompleteByIdRequest request) {
        check(request);
        taskService.changeStatusById(
                request.getUserId(),
                request.getId(),
                Status.COMPLETED
        );
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeByIndex(@NotNull TaskCompleteByIndexRequest request) {
        check(request);
        taskService.changeStatusByIndex(
                request.getUserId(),
                request.getIndex(),
                Status.COMPLETED
        );
        return new TaskCompleteByIndexResponse();
    }

    @NotNull
    @Override
    public TaskCreateResponse create(@NotNull TaskCreateRequest request) {
        check(request);
        return new TaskCreateResponse(
                taskService.create(
                        request.getUserId(),
                        request.getName(),
                        request.getDescription()
                )
        );
    }

    @NotNull
    @Override
    public TaskGetByIdResponse getById(@NotNull TaskGetByIdRequest request) {
        check(request);
        return new TaskGetByIdResponse(
                taskService.findOneById(
                        request.getUserId(),
                        request.getId())
        );
    }

    @NotNull
    @Override
    public TaskGetByIndexResponse getByIndex(@NotNull TaskGetByIndexRequest request) {
        check(request);
        return new TaskGetByIndexResponse(
                taskService.findOneByIndex(
                        request.getUserId(),
                        request.getIndex()
                )
        );
    }

    @NotNull
    @Override
    public TaskListResponse list(@NotNull TaskListRequest request) {
        check(request);
        return new TaskListResponse(
                taskService.findAll(
                        request.getUserId(),
                        request.getSortType()
                )
        );
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeById(@NotNull TaskRemoveByIdRequest request) {
        check(request);
        taskService.removeById(request.getUserId(), request.getId());
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeByIndex(@NotNull TaskRemoveByIndexRequest request) {
        check(request);
        taskService.removeByIndex(request.getUserId(), request.getIndex());
        return new TaskRemoveByIndexResponse();
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startById(@NotNull TaskStartByIdRequest request) {
        check(request);
        taskService.changeStatusById(
                request.getUserId(),
                request.getId(),
                Status.IN_PROGRESS
        );
        return new TaskStartByIdResponse();
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startByIndex(@NotNull TaskStartByIndexRequest request) {
        check(request);
        taskService.changeStatusByIndex(
                request.getUserId(),
                request.getIndex(),
                Status.IN_PROGRESS
        );
        return new TaskStartByIndexResponse();
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateById(@NotNull TaskUpdateByIdRequest request) {
        check(request);
        taskService.updateById(
                request.getUserId(),
                request.getId(),
                request.getName(),
                request.getDescription()
        );
        return new TaskUpdateByIdResponse();
    }

    @Override
    public @NotNull TaskUpdateByIndexResponse updateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        check(request);
        taskService.updateByIndex(
                request.getUserId(),
                request.getIndex(),
                request.getName(),
                request.getDescription()
        );
        return new TaskUpdateByIndexResponse();
    }

}
