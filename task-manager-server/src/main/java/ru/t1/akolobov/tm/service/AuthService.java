package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.service.IAuthService;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.user.*;
import ru.t1.akolobov.tm.model.User;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull IUserService userService,
            @NotNull IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findByLogin(login))
                .orElseThrow(IncorrectLoginOrPasswordException::new);
        if (user.isLocked() || !HashUtil.salt(propertyService, password).equals(user.getPasswordHash()))
            throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public User check(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findByLogin(login))
                .orElseThrow(IncorrectLoginOrPasswordException::new);
        if (user.isLocked() || !HashUtil.salt(propertyService, password).equals(user.getPasswordHash()))
            throw new IncorrectLoginOrPasswordException();
        return user;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    @NotNull
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    @NotNull
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return Optional.ofNullable(userService.findOneById(userId))
                .orElseThrow(AccessDeniedException::new);
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionDeniedException();
    }

}
