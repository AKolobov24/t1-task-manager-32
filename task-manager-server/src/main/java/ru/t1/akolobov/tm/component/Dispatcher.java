package ru.t1.akolobov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.server.Operation;
import ru.t1.akolobov.tm.dto.request.AbstractRequest;
import ru.t1.akolobov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
