package ru.t1.akolobov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.*;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.endpoint.*;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.repository.ProjectRepository;
import ru.t1.akolobov.tm.repository.TaskRepository;
import ru.t1.akolobov.tm.repository.UserRepository;
import ru.t1.akolobov.tm.service.*;
import ru.t1.akolobov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            userRepository,
            projectRepository,
            taskRepository,
            propertyService
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(
            projectService,
            taskService,
            userService,
            authService
    );

    private final Backup backup = new Backup(domainService);

    private final Server server = new Server(this);

    private final SystemEndpoint systemEndpoint = new SystemEndpoint(this);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private final DomainEndpoint domainEndpoint = new DomainEndpoint(this);

    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clear);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::create);
        server.registry(ProjectGetByIdRequest.class, projectEndpoint::getById);
        server.registry(ProjectGetByIndexRequest.class, projectEndpoint::getByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::list);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindFromProject);
        server.registry(TaskGetByProjectIdRequest.class, taskEndpoint::getByProjectId);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clear);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::create);
        server.registry(TaskGetByIdRequest.class, taskEndpoint::getById);
        server.registry(TaskGetByIndexRequest.class, taskEndpoint::getByIndex);
        server.registry(TaskListRequest.class, taskEndpoint::list);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfile);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYaml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveDataYaml);
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        projectService.create("1", "TEST PROJECT", Status.IN_PROGRESS);
        projectService.create("1", "DEMO PROJECT", Status.COMPLETED);
        projectService.create("2", "BETA PROJECT", Status.NOT_STARTED);
        projectService.create("2", "BEST PROJECT", Status.IN_PROGRESS);
        taskService.create("1", "TEST TASK", Status.IN_PROGRESS);
        taskService.create("1", "DEMO TASK", Status.COMPLETED);
        taskService.create("2", "BETA TASK", Status.NOT_STARTED);
        taskService.create("2", "BEST TASK", Status.IN_PROGRESS);
        userService.create("user1", "user1", "user@mail.ru").setId("2");
        userService.create("akolobov", "akolobov", Role.ADMIN).setId("1");
    }

    public void run() {
        initPid();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

}
