package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.akolobov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.akolobov.tm.dto.response.ApplicationVersionResponse;

public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setName(propertyService.getApplicationAuthor());
        response.setEmail(propertyService.getAuthorEmail());
        return response;
    }

    @Override
    @NotNull
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
