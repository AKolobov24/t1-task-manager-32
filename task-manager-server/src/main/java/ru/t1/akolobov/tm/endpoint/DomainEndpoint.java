package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.akolobov.tm.api.service.IDomainService;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.FileFormat;
import ru.t1.akolobov.tm.enumerated.Role;

public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    private final IDomainService domainService;

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.domainService = serviceLocator.getDomainService();
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadBackup(@NotNull DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveBackup(@NotNull DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        domainService.loadData(FileFormat.JSON);
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        domainService.saveData(FileFormat.JSON);
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse loadDataJsonJaxb(@NotNull DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataJaxb(FileFormat.JSON);
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse saveDataJsonJaxb(@NotNull DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataJaxb(FileFormat.JSON);
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        domainService.loadData(FileFormat.XML);
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        domainService.saveData(FileFormat.XML);
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse loadDataXmlJaxb(@NotNull DataXmlLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataJaxb(FileFormat.XML);
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse saveDataXmlJaxb(@NotNull DataXmlSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataJaxb(FileFormat.XML);
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse loadDataYaml(@NotNull DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        domainService.loadData(FileFormat.YAML);
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse saveDataYaml(@NotNull DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        domainService.saveData(FileFormat.YAML);
        return new DataYamlSaveFasterXmlResponse();
    }

}
